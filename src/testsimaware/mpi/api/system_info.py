from dataclasses import dataclass


@dataclass
class SystemInfo:
    """
    Information collected from the system.
    The characteristics of the system depend on the physical machine where the agent is running, and are not
    expected to change during the execution of the agent.

    Attributes
    ----------
    system:
        the name of the operating system
    node:
        the name of the node
    release:
        the release of the operating system
    version:
        the version of the operating system
    machine:
        the machine type
    processor:
        the processor used
    """

    system: str
    node: str
    release: str
    version: str
    machine: str
    processor: str
