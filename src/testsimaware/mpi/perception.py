import platform
from threading import Event, Thread
from typing import Callable, Literal

import psutil

from .api import SystemInfo, SystemStatus
from .util import Publisher

Events = Literal["system_status"]


class Perception(Publisher):
    """
    Perception class.
    It is responsible for collecting data from the system and publishing it to the subscribers.

    Attributes
    ----------
    _system_info:
        the system information
    _system_statuses:
        the list of system statuses. The last element is the most recent one.
    _check_interval:
        the interval between two checks of the system
    _loop_thread:
        the thread that runs every check_interval seconds to collect the system status
    _stop_thread:
        event used to stop the loop thread
    """

    def __init__(self, check_interval: float = 1):
        super().__init__()
        self._system_info: SystemInfo = self._get_system_info()
        self._system_statuses: list[SystemStatus] = []
        self._check_interval = check_interval
        self._loop_thread = Thread(target=self._loop)
        self._stop_thread = Event()

    def start(self):
        """
        Start monitoring the system.
        Each time a new system status is available, it will be published to the subscribers.
        This happens at a frequency specified by the check_interval parameter.
        """
        if not self._loop_thread.is_alive():
            self._stop_thread.clear()
            self._loop_thread = Thread(target=self._loop)
            self._loop_thread.start()

    def stop(self):
        """
        Stop monitoring the system.
        No new system status will be published to the subscribers.
        The Perception object can be restarted by calling the start method.
        """
        if self._loop_thread.is_alive():
            self._stop_thread.set()

    def _loop(self):
        """
        Loop that monitors the system and publishes new system statuses to the subscribers.
        When the stop method is called, the loop will stop, and the method will return.
        To restart the loop, call the start method.
        """
        while True:
            self._system_statuses.append(self._get_system_status())
            self.notify("system_status", self.system_status)
            if self._stop_thread.wait(self._check_interval):
                break

    def _get_system_info(self) -> SystemInfo:
        """
        Produce a SystemInfo object containing information about the system.
        This kind of information does not change over time, so it is not necessary to update it.

        Returns
        -------
        information about the system
        """
        return SystemInfo(
            machine=platform.machine(),
            node=platform.node(),
            release=platform.release(),
            system=platform.system(),
            version=platform.version(),
            processor=platform.processor(),
        )

    def _get_system_status(self) -> SystemStatus:
        """
        Produce a SystemStatus object containing information about the system.
        This kind of information changes over time, so it is necessary to update it.

        Returns
        -------
        current status of the system
        """
        return SystemStatus(
            cpu_percent=psutil.cpu_percent(),
            disk_usage_percent=psutil.disk_usage("/").percent,
            virtual_memory_percent=psutil.virtual_memory().percent,
        )

    def add(self, event: Events, callback: Callable[[SystemStatus], None]):
        """
        Add a callback to the publisher.
        The callback must accept a SystemStatus object as parameter, corresponding to the new system status that
        triggered the event.

        Parameters
        ----------
        event:
            event identifier the callback will be invoked for
            Available events:

            - system_status
        callback:
            the callback to add
        """
        self._add(event, callback)

    def remove(self, event: Events, callback: Callable[[SystemStatus], None]):
        """
        Remove a callback from the publisher.

        Parameters
        ----------
        event:
            event identifier the callback was attached to.
            Available events:

            - system_status
        callback:
            the callback to remove
        """
        self._remove(event, callback)

    def notify(self, event: str, *data: SystemStatus):
        """
        Notify the subscribers that a new system status is available.

        Parameters
        ----------
        event:
            event identifier.
        data:
            new system status
        """
        self._notify(event, *data)

    @property
    def system_statuses(self) -> list[SystemStatus]:
        """
        Return the list of system statuses collected so far.

        Returns
        -------
        the list of system statuses collected so far
        """
        return self._system_statuses

    @property
    def system_status(self) -> SystemStatus:
        """
        Return the most recent system status.
        To get the list of all the system statuses collected so far, use the system_statuses property.

        Returns
        -------
        the most recent system status
        """
        return self._system_statuses[-1]

    @property
    def system_info(self) -> SystemInfo:
        """
        Return the system information.

        Returns
        -------
        the system information
        """
        return self._system_info
