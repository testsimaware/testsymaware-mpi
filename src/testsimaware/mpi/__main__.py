import argparse
from typing import TYPE_CHECKING

from .perception import Perception

try:
    from ._version import __version__
except ImportError:
    __version__ = "unknown"

if TYPE_CHECKING:
    from argparse import Namespace

    class CommandLineArgs(Namespace):
        """Command line arguments type hint.

        Attributes:
        ----------
        check_interval:
            interval between checks.
        number_checks:
            number of checks to perform before stopping the application.
        """

        check_interval: int
        number_checks: int


def parse_args() -> "CommandLineArgs":
    """
    Parse command line arguments for the script.

    Returns:
    --------
    args:
        a namespace containing the parsed arguments. Namely:

        - number_of_agents : The number of agents in the simulation.
        - simulation_iterations : The number of iterations to run the simulation for.
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("--version", action="version", version=__version__)
    parser.add_argument("-i", "--check_interval", help="Interval between checks in seconds", type=int, default=1)
    parser.add_argument(
        "-n",
        "--number_checks",
        help="Number of checks to perform before stopping the application",
        type=int,
        default=10,
    )
    return parser.parse_args()  # type: ignore


def main():
    """
    Main entry point for the application.
    Can be executed by the command line by running `python -m testsimaware.mpi`.
    """
    args = parse_args()
    perception = Perception(check_interval=args.check_interval)

    def print_system_status(x):
        print(f"System status: {x}")

    perception.add("system_status", print_system_status)
    perception.start()

    while len(perception.system_statuses) < args.number_checks // 2:
        pass

    perception.stop()

    print("Halfway there!")

    perception.start()

    while len(perception.system_statuses) < args.number_checks:
        pass

    perception.stop()

    print("Done!")


if __name__ == "__main__":
    main()
