# pylint: disable=import-error,unused-import,missing-function-docstring,missing-class-docstring,protected-access,unnecessary-lambda-assignment,unnecessary-lambda,redefined-outer-name
from time import sleep

from pytest_mock import MockerFixture, mocker

from testsimaware.mpi import Perception, SystemInfo, SystemStatus


class TestPerception:
    def test_constructor(self):
        perception = Perception()
        assert perception._check_interval == 1
        assert perception._stop_thread.is_set() is False
        assert isinstance(perception._system_info, SystemInfo)

    def test_add_callback(self):
        callback = lambda x: print(x)
        perception = Perception()
        perception.add("system_status", callback)
        assert len(perception._callbacks["system_status"]) == 1
        assert callback in perception._callbacks.get("system_status", set())

    def test_add_callback_no_duplicate(self):
        callback = lambda x: print(x)
        perception = Perception()
        perception.add("system_status", callback)
        perception.add("system_status", callback)
        assert len(perception._callbacks.get("system_status", set())) == 1

    def test_remove_callback(self):
        callback = lambda x: print(x)
        perception = Perception()
        perception.add("system_status", callback)
        perception.remove("system_status", callback)
        assert len(perception._callbacks.get("system_status", set())) == 0

    def test_remove_callback_not_present(self):
        callback = lambda x: print(x)
        perception = Perception()
        perception.remove("system_status", callback)
        assert len(perception._callbacks.get("system_status", set())) == 0

    def test_start(self):
        perception = Perception()
        perception.start()
        assert perception._stop_thread.is_set() is False
        assert perception._loop_thread.is_alive() is True
        perception.stop()

    def test_stop(self):
        perception = Perception()
        perception.start()
        perception.stop()
        assert perception._stop_thread.is_set() is True
        sleep(0.01)
        assert perception._loop_thread.is_alive() is False

    def test_get_system_info(self):
        perception = Perception()
        system_info = perception._get_system_info()
        assert isinstance(system_info, SystemInfo)

    def test_get_system_status(self):
        perception = Perception()
        system_status = perception._get_system_status()
        assert isinstance(system_status, SystemStatus)

    def test_loop(self, mocker: MockerFixture):
        perception = Perception(check_interval=0)

        def side_effect(_):
            perception._stop_thread.set()

        callback = mocker.Mock(side_effect=side_effect)
        perception.add("system_status", callback)

        perception._loop()

        callback.assert_called_once()
        callback.assert_called_with(perception.system_status)
